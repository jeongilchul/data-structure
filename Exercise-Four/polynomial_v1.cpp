#include <stdio.h>
#include <stdlib.h>

struct listNode {
	int coef;
	int ex;
	struct listNode *NextPtr;
};

typedef struct listNode ListNode;
typedef ListNode *ListNodePtr;

// function prototype 

void insert( ListNodePtr *sPtr, int c, int e )
{
	printf("insert function\n");
	ListNodePtr currentPtr = NULL, previousPtr = NULL;
	ListNodePtr newPtr = malloc(sizeof(ListNode));
	if( newPtr != NULL )
	{
		newPtr->coef = c;
		newPtr->ex = e;
		newPtr->NextPtr = NULL;
		currentPtr = *sPtr;
		while( currentPtr != NULL && e > currentPtr->ex ){
			previousPtr = currentPtr;
			currentPtr = currentPtr->NextPtr;
		}
		if( previousPtr == NULL ){
			newPtr->NextPtr = *sPtr;
			*sPtr = newPtr;
		}
		else{
			previousPtr->NextPtr = newPtr;
			newPtr->NextPtr = currentPtr;
		}
	}
	else{
		printf("%s\n", "No memory available");
	}	
}

void show( ListNodePtr currentPtr ){
	if( currentPtr == NULL ) {
		puts("Empty list");
	}
	else {
		puts("Printing list\n");
		while( currentPtr != NULL ){
			printf("%d ==> ", currentPtr->coef);
			printf("%d ===> ", currentPtr->ex);
			currentPtr = currentPtr->NextPtr;
		}
		puts("End Of List");
	}
}

int main( void )
{
	ListNodePtr eq1 = NULL; 
	ListNodePtr eq2 = NULL;
	insert(&eq1, 3, 9);
	show(eq1);
	
	getchar();
	return 0;
}
