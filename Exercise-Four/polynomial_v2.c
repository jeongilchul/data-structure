#include <stdio.h>
#include <stdlib.h>

struct listNode {
	int coef;
	int ex;
	struct listNode *NextPtr;
};

typedef struct listNode ListNode;
typedef ListNode *ListNodePtr;

// function prototype 

void insert( ListNodePtr *sPtr, int c, int e )
{
	printf("insert function\n");
	ListNodePtr currentPtr = NULL, previousPtr = NULL;
	ListNodePtr newPtr = malloc(sizeof(ListNode));
	if( newPtr != NULL )
	{
		newPtr->coef = c;
		newPtr->ex = e;
		newPtr->NextPtr = NULL;
		currentPtr = *sPtr;
		while( currentPtr != NULL && e < currentPtr->ex ){
			previousPtr = currentPtr;
			currentPtr = currentPtr->NextPtr;
		}
		if( previousPtr == NULL ){
			newPtr->NextPtr = *sPtr;
			*sPtr = newPtr;
		}
		else{
			previousPtr->NextPtr = newPtr;
			newPtr->NextPtr = currentPtr;
		}
	}
	else{
		printf("%s\n", "No memory available");
	}	
}

void show( ListNodePtr currentPtr ){
	if( currentPtr == NULL ) {
		puts("Empty list");
	}
	else {
		puts("Printing list\n");
		while( currentPtr != NULL ){
			printf("%d^", currentPtr->coef);
			printf("%d ", currentPtr->ex);
			currentPtr = currentPtr->NextPtr;
			if( currentPtr != NULL )
			{
				if( currentPtr->coef > 0 )
					printf("+");
			}
		}
		puts("");
	}
}

void basicOP(ListNodePtr *eq1, ListNodePtr *eq2, ListNodePtr *result, int mode )
{
	ListNodePtr currentPtr1 = NULL;
	ListNodePtr currentPtr2 = NULL;
	currentPtr1 = *eq1;
	currentPtr2 = *eq2;
	int coef, exp;
	int loop = 0;
	int flag = 0; 
	while( 1 )
	{
		/*
		if( currentPtr1->ex == currentPtr2->ex )
		{
			// add
			currentPtr2->NextPtr = currentPtr2->NextPtr;
		}
		else if( currentPtr1->ex > currentPtr->ex )
		{
			// insert
			currentPtr2->NextPtr = currentPtr2->NextPtr;
		}
		if( currentPtr2 == NULL )
		{
			break;
		}
		else if( currentPtr2 != NULL && currentPtr1 == NULL )
		{
			// load the rest to currentPtr1
			while( currentPtr2 != NULL )
			{
				// insert
			}
		}
		*/
		if( currentPtr1->ex == currentPtr2->ex )
		{
			exp = currentPtr1->ex;
			if( mode )
				coef = currentPtr1->coef + currentPtr2->coef;
			else
				coef = currentPtr1->coef - currentPtr2->coef;
			insert( result,coef, exp );
			currentPtr2 = currentPtr2->NextPtr;
			flag = 1;
			currentPtr1 = currentPtr1->NextPtr;
		}
		else if( currentPtr1->ex < currentPtr2->ex )
		{
			exp = currentPtr2->ex;
			coef = currentPtr2->coef;
			insert( result, coef, exp );
			currentPtr2 = currentPtr2->NextPtr;
			flag = 2;
		}
		if( flag == 2 || flag == 0 )
		{
			exp = currentPtr1->ex;
			coef = currentPtr1->coef;
			insert( result, coef, exp );
			flag = 0;
			currentPtr1 = currentPtr1->NextPtr;
		}
		else if( flag == 1 )
		{
			flag = 0;
		}
		if( currentPtr2 == NULL )
			break;
		else if( currentPtr2 != NULL && currentPtr1 == NULL )
		{
			while( currentPtr2 != NULL )
			{
				exp = currentPtr2->ex;
				coef = currentPtr2->coef;
				insert( result, coef, exp );
				currentPtr2 = currentPtr2->NextPtr;
			}
			break;
		}
		// currentPtr1 = currentPtr1->NextPtr;
	}	
}

void advanceOp(ListNodePtr *eq1, ListNodePtr *eq2, ListNodePtr *result, int mode )
{
	ListNodePtr currentPtr1 = NULL;
	ListNodePtr currentPtr2 = NULL;
	currentPtr1 = *eq1;
	currentPtr2 = *eq2;
	int coef, exp;
	int loop = 0;
	int flag = 0;
	
	while( 1 )
	{
		if( currentPtr1->coef == currentPtr2->coef )
		{
			if( mode )
				coef = currentPtr1->ex * currentPtr2->ex;
			else
				coef = currentPtr1->ex - currentPtr2->ex;
			insert( result,coef, exp );
			currentPtr2 = currentPtr2->NextPtr;
			flag = 1;
			currentPtr1 = currentPtr1->NextPtr;
		}
		
	} 
}

void deleteList( ListNodePtr *start ) {
	ListNodePtr previousPtr;
	ListNodePtr currentPtr;
	ListNodePtr tempPtr;
	
	previousPtr = *start;
	currentPtr = previousPtr->NextPtr;
	while( currentPtr != NULL ){
		tempPtr = currentPtr;
		previousPtr->NextPtr = currentPtr->NextPtr;
		currentPtr = currentPtr->NextPtr;
		free( tempPtr );
	}
	if( previousPtr != NULL )
	{
		tempPtr = previousPtr;
		*start = NULL;
		free(tempPtr);
	}
}	
int main( void )
{
	ListNodePtr eq1 = NULL; 
	ListNodePtr eq2 = NULL;
	ListNodePtr result = NULL;
	int choice, c, e;
	choice = c = e = 0;
	while( choice != -1 )
	{
		scanf("%d%d", &c, &e);
		insert(&eq1, c, e);
		scanf("%d", &choice);
	}
	choice = 0;
	while( choice != -1 )
	{
		scanf("%d%d", &c, &e);
		insert(&eq2, c, e);
		scanf("%d", &choice);
	}
	puts("adding...");
	
	show(eq1);
	show(eq2);
	basicOP(&eq1, &eq2, &result, 1);
	show(result);
	puts("deleting...");
	deleteList(&result);
	basicOP(&eq1, &eq2, &result, 0);
	show(result);
	
	// before calling the advanceOp, first sort eq1 and eq2 with coef
	puts("ended");
	getchar();
	return 0;
}
